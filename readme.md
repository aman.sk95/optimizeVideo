```
USAGE
       optimizeVideo [videos]

EXCLUSION
       The original file won't be substituted if its format is webm and the compression
       gain would be less than 15%.
